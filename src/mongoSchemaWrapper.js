'use strict'

import { Schema } from 'mongoose'
import MONGO_CONFIG from './MONGO_CONFIG'

const {
  CONFIG: { REPLICASET_COUNT }
} = MONGO_CONFIG

const maxWriteCount = REPLICASET_COUNT ? (REPLICASET_COUNT + 1) : 'majority'

const DEFAULT_OPTIONS = {
  timestamps: true
}

export default function mongoSchemaWrapper (schemaObject, options = {}) {
  const schemaOptions = { ...DEFAULT_OPTIONS, ...options }

  // Additional Feature: writeConcern.w = 'all'
  if (schemaOptions.writeConcern && schemaOptions.writeConcern.w === 'all') {
    schemaOptions.writeConcern.w = maxWriteCount
  }

  const schema = new Schema(schemaObject, options)
  return schema
}
