'use strict'

import mongoose from 'mongoose'
import MongoError from './MongoError'
import {
  DEFAULT_SKIP,
  DEFAULT_LIMIT,
  DEFAULT_SORT
} from './CONSTANTS'

const { ERROR_TYPE } = MongoError

export default class MongoModel {
  constructor (modelName = '', Schema) {
    this.ModelName = modelName
    this.Schema = Schema
    this.MongooseModel = mongoose.model(modelName, Schema)

    // Method Hard Binding
    this.getCount = this.getCount.bind(this)

    this.createOne = this.createOne.bind(this)
    this.createMany = this.createMany.bind(this)
    this.replaceAll = this.replaceAll.bind(this)

    this.findOne = this.findOne.bind(this)
    this.findMany = this.findMany.bind(this)
    this.findById = this.findById.bind(this)
    this.findOneBy = this.findOneBy.bind(this)
    this.findManyBy = this.findManyBy.bind(this)

    this.updateOne = this.updateOne.bind(this)
    this.updateMany = this.updateMany.bind(this)
    this.updateById = this.updateById.bind(this)
    this.updateOneBy = this.updateOneBy.bind(this)
    this.updateManyBy = this.updateManyBy.bind(this)

    this.remove = this.remove.bind(this)
    this.removeById = this.removeById.bind(this)

    this.list = this.list.bind(this)
    this.search = this.search.bind(this)
  }

  async getCount (query = {}) {
    const count = await this.MongooseModel.count(query)
    return count
  }

  async createOne (attrs = {}) {
    const instance = await this.MongooseModel.create(attrs)
    return instance
  }

  async createMany (attrs = []) {
    if (!attrs.length) { return [] }
    const instances = await this.MongooseModel.create(attrs)
    return instances
  }

  async replaceAll (attrs = []) {
    await this.remove({})
    const instances = await this.createMany(attrs)
    return instances
  }

  async findOne (query = {}, projection = {}, options = {}) {
    const instance = await this.MongooseModel.findOne(query, projection, options)
    return instance
  }

  async findMany (query = {}, projection = {}, options = {}) {
    const instances = await this.MongooseModel.find(query, projection, options)
    return instances
  }

  async findById (id, projections = {}, options = {}) {
    const instance = await this.MongooseModel.findById(id, projections, options)
    if (!instance) {
      const error = {
        message: 'Document Not Found',
        name: ERROR_TYPE.DocumentNotFoundError,
        errors: { id, options }
      }
      throw new MongoError(error)
    }
    return instance
  }

  async findOneBy (key = '', value, projection = {}, options = {}) {
    const query = { [key]: value }
    const instance = await this.findOne(query, projection, options)
    return instance
  }

  async findManyBy (key = '', value, projection = {}, options = {}) {
    const query = { [key]: value }
    const instances = await this.findMany(query, projection, options)
    return instances
  }

  async updateOne (query = {}, updateObj = {}, options = {}) {
    const updateOptions = { ...options, new: true, rawResult: true }
    const updateResponse = await this.MongooseModel.findOneAndUpdate(query, updateObj, updateOptions).orFail()
    const { value: instance } = updateResponse
    return instance
  }

  async updateMany (query = {}, updateObj = {}, options = {}) {
    const updateResponse = await this.MongooseModel.updateMany(query, updateObj, options).orFail()
    return updateResponse
  }

  async updateById (id, updateObj = {}, options = {}) {
    const query = { _id: id }
    const instance = await this.updateOne(query, updateObj, options)
    if (!instance) {
      const error = {
        message: 'Document Not Found',
        name: ERROR_TYPE.DocumentNotFoundError,
        errors: { id, options }
      }
      throw new MongoError(error)
    }
    return instance
  }

  async updateOneBy (key = '', value, updateObj = {}, options = {}) {
    const query = { [key]: value }
    const instance = await this.updateOne(query, updateObj, options)
    return instance
  }

  async updateManyBy (key = '', value, updateObj = {}, options = {}) {
    const query = { [key]: value }
    const instances = await this.updateMany(query, updateObj, options)
    return instances
  }

  async remove (query = {}, options = {}) {
    const removeOptions = { ...options, rawResult: true }
    const removeResponse = await this.MongooseModel.deleteMany(query, removeOptions)
    const { value: instance = [] } = removeResponse || {}
    return instance
  }

  async removeById (id, options = {}) {
    const removeOptions = { ...options, rawResult: true }
    const removeResponse = await this.MongooseModel.findByIdAndRemove(id, removeOptions).orFail()
    const { value: instance } = removeResponse
    return instance
  }

  async list (projection = {}, options = {}) {
    const instances = await this.findMany({}, projection, options)
    return instances
  }

  async search (attrs) {
    const { query = {}, projections = {}, options = {} } = attrs

    const searchOptions = {
      skip: options.skip || DEFAULT_SKIP,
      limit: options.limit || DEFAULT_LIMIT,
      sort: options.sort || DEFAULT_SORT
    }

    const promises = [
      this.getCount(query),
      this.findMany(query, projections, searchOptions)
    ]

    const [countResponse, findResponse] = await Promise.allSettled(promises)
    const countError = countResponse.status === 'rejected'
    const findError = findResponse.status === 'rejected'

    if (findError) { throw findResponse.reason }

    const { value: count } = countResponse
    const { value: instances } = findResponse
    const data = {
      _meta: {
        query,
        projections,
        options: searchOptions,
        totalRecords: countError ? null : count,
        recordsCount: instances.length
      },
      records: instances
    }
    return data
  }
}
