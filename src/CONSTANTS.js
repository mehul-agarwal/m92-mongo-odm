'use strict'

export const DEFAULT_SKIP = 0

export const DEFAULT_LIMIT = 10

export const DEFAULT_SORT = '-_id'
