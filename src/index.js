'use strict'

import * as mongooseExports from 'mongoose'
import mongoSchemaWrapper from './mongoSchemaWrapper'
import MongoModel from './MongoModel'
import MongoError from './MongoError'
import MONGO_CONFIG from './MONGO_CONFIG'
import * as MONGO_CONSTANSTS from './CONSTANTS'

const { default: mongoose } = mongooseExports
export const exportProps = {
  ...mongooseExports,
  mongoose,
  mongoSchemaWrapper,
  MongoModel,
  MongoError,
  MONGO_CONFIG,
  MONGO_CONSTANSTS
}

module.exports = exportProps
