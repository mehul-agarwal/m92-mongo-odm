'use strict'

import autoBind from 'auto-bind'
import ResponseBody from './ResponseBody'

const ERROR_TYPE = {
  MongooseError: 'MongooseError',
  CastError: 'CastError',
  DisconnectedError: 'DisconnectedError',
  DivergentArrayError: 'DivergentArrayError',
  DocumentNotFoundError: 'DocumentNotFoundError',
  ValidatorError: 'ValidatorError',
  ValidationError: 'ValidationError',
  MissingSchemaError: 'MissingSchemaError',
  ObjectExpectedError: 'ObjectExpectedError',
  ObjectParameterError: 'ObjectParameterError',
  OverwriteModelError: 'OverwriteModelError',
  ParallelSaveError: 'ParallelSaveError',
  StrictModeError: 'StrictModeError',
  VersionError: 'VersionError',
  RuntimeError: 'RuntimeError'
}

const STATUS_CODE_MAP = {
  MongooseError: 500,
  CastError: 422,
  DisconnectedError: 500,
  DivergentArrayError: 409,
  DocumentNotFoundError: 404,
  ValidatorError: 422,
  ValidationError: 422,
  MissingSchemaError: 500,
  ObjectExpectedError: 422,
  ObjectParameterError: 500,
  OverwriteModelError: 500,
  ParallelSaveError: 409,
  StrictModeError: 500,
  VersionError: 409,
  RuntimeError: 500
}

const ERROR_NAME = 'MongoError'
const ERROR_CLASSIFICATION = 'DATABASE_ERROR'

const CAN_CAPTURE = typeof Error.captureStackTrace === 'function'
const CAN_STACK = !!new Error().stack

export default class MongoError extends Error {
  constructor (error) {
    const {
      _isMongoError,
      message,
      msg,
      name,
      type,
      statusCode,
      errors,
      error: err,
      stack
    } = error

    const errorHasKeys = !!Object.keys(error).length
    const _msg = msg || message

    super(_msg)

    this._isMongoError = true
    this.name = ERROR_NAME
    this.classification = ERROR_CLASSIFICATION

    this.message = _msg
    this.msg = _msg

    this.statusCode = (_isMongoError && statusCode) || STATUS_CODE_MAP[name] || 500
    this.type = (_isMongoError && type) || name

    this.error = (!_isMongoError && errorHasKeys && error) || errors || err || undefined
    const thisErrorHasKeys = !!Object.keys(this.error || {}).length
    if (!thisErrorHasKeys) { this.error = undefined }

    this.stack = stack || (
      (CAN_CAPTURE && Error.captureStackTrace(this, MongoError)) ||
      (CAN_STACK && new Error().stack) ||
      undefined
    )

    autoBind(this)
  }

  getResponseBody () {
    const { statusCode, message } = this
    const error = this.toJSON()

    const { NODE_ENV } = process.env
    error.stack = (NODE_ENV === 'production' && undefined) || error.stack

    return new ResponseBody(statusCode, message, undefined, error)
  }

  toJSON () {
    const { toJSON, ...rest } = this
    return JSON.parse(JSON.stringify(rest))
  }

  static get STATUS_CODE_MAP () { return STATUS_CODE_MAP }

  static get ERROR_TYPE () { return ERROR_TYPE }
}
